# MKKeyboardExtension

#### 项目介绍
一个键盘的扩展主要用来展示键盘上面的输入视图

使用方式
```swift

//设置显示的弹出样式
[MKKeyboardView setDefaultMKKeyboardShowStyle:MKKeyboardShowStyleDefault];
//设置显示什么样式的视图
[MKKeyboardView setDefaultMKKeyboardShowViewStyle:MKKeyboardShowViewStyleDefault];
//视图上一些可以设置的部分
[MKKeyboardView setDefaultShowViewPlaceholder:@"说些什么吧" buttonTitle:@"发布" buttonTitleFont:14.f buttonBackGroundColor:[UIColor orangeColor] buttonTitleColor:[UIColor whiteColor]];
//完成时候的回调
[MKKeyboardView showMkKeyBoardyViewSureCallBack:^(id  _Nullable backObject) {
NSLog(@"backObject : %@",(NSString *)backObject);
}];
//显示视图
[MKKeyboardView show];

//注意点 默认样式一旦发生改变那么全局的都会变 如果你在一个项目当中要使用不同的弹出形式 建议每一个弹框 都要设置下 弹出样式和动画样式

```
开放的属性和类方法

```swift

/设置初始的键盘头弹出样式
+ (void)setDefaultMKKeyboardShowStyle:(MKKeyboardShowStyle)keyboardShowStyle;

//设置初始的键盘头视图的样式
+ (void)setDefaultMKKeyboardShowViewStyle:(MKKeyboardShowViewStyle)showViewStyle;

/**
设置默认的一些必要参数

@param placeholder 提示内容
@param buttonTitle 按钮的名称
@param buttonTitleFont 按钮名字的字体大小
@param buttonBackGroundColor 按钮的背景色
@param buttonTitleColor 按钮的字体颜色
*/
+ (void)setDefaultShowViewPlaceholder:(NSString *)placeholder
buttonTitle:(NSString *)buttonTitle
buttonTitleFont:(CGFloat)buttonTitleFont
buttonBackGroundColor:(UIColor *)buttonBackGroundColor
buttonTitleColor:(UIColor *)buttonTitleColor;

#pragma mark - 本类的初始化方法主要用于显示弹框

/**
默认的弹出样式

@param content 返回的文本
*/
+ (void)showMkKeyBoardyViewSureCallBack:(void(^_Nullable)(id _Nullable backObject))content;

/**
带图片选择的弹出样式

@param content 返回的文本
@param imageSelected 图片选择被点击
*/
+ (void)showMkKeyBoardyImageViewSureCallBack:(void(^_Nullable)(id _Nullable backObject))content
imageSelected:(void(^_Nullable)(void))imageSelected;

//展示我要的界面
+ (void)show;

#pragma mark - 可以自已定义的属性
//设置初始的键盘头弹出样式
@property (assign, nonatomic,setter = setDefaultShowStyle:) MKKeyboardShowStyle keyboardShowStyle;
//设置初始的键盘头视图的样式
@property (assign, nonatomic,setter = setDefaultViewStyle:) MKKeyboardShowViewStyle showViewStyle;
//提示内容
@property (strong, nonatomic) NSString *placeholderString;
//按钮的名称
@property (strong, nonatomic) NSString *buttonTitle;
//按钮名字的字体大小
@property (assign, nonatomic) CGFloat buttonTitleFont;
//按钮的背景色
@property (strong, nonatomic) UIColor *buttonBackGroundColor;
//按钮的字体颜色
@property (strong, nonatomic) UIColor *buttonTitleColor;

```

